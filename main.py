import win32gui
import win32con
import time
from controller import AzurController
from screen import Screen


def main():
    mhwnd = win32gui.FindWindow(None, "Untitled - Notepad")
    chwnd = win32gui.GetWindow(mhwnd, win32con.GW_CHILD)

    # controller = AzurController(chwnd)
    # controller.press_key(0x30)
    # controller.click(20, 0)

    screen = Screen(chwnd)
    while True:
        print(screen.current_screen())
        time.sleep(1)


if __name__ == "__main__":
    main()
