import win32con
import win32api


class Controller:
    """
    Sends inputs to target window.

    :param hwnd: A handle to a window.

    :ivar hwnd: A handle to a window.
    """

    def __init__(self, hwnd):
        self.hwnd = hwnd

    def press_key(self, key):
        """
        Sends key press to window.
        :param hex key: Hex code of key to press.
        """
        win32api.PostMessage(self.hwnd, win32con.WM_CHAR, key)

    def click(self, x, y):
        """
        Sends mouse left click to window.
        :param x: X coordinate of mouse click
        :param y: Y Coordinate of mouose click
        """
        lparam = win32api.MAKELONG(x, y)
        win32api.PostMessage(self.hwnd, win32con.WM_LBUTTONDOWN, win32con.MK_LBUTTON, lparam)
        win32api.PostMessage(self.hwnd, win32con.WM_LBUTTONUP, win32con.MK_LBUTTON, lparam)


class AzurController(Controller):
    def next(self):
        """
        Click on the correct element to advance to the next screen.
        """
        pass
